use once_cell::sync::Lazy;
use reqwest::Client;

static HTTP: Lazy<Client> = Lazy::new(|| Client::new());

static TOKEN: &str = include_str!("../influx-token.txt");

pub async fn make_request(body: String) -> color_eyre::Result<()> {
    let response = HTTP
    .post("http://127.0.0.1:8086/api/v2/write?org=pluth&bucket=pluth")
    .header("Authorization", format!("Token {TOKEN}"))
    .body(body).send().await?;
    response.error_for_status()?;
    Ok(())
}
