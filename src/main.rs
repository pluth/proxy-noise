use std::net::SocketAddr;

use noise::responder_handshake;
use noise_protocol::U8Array;
use noise_rust_crypto::sensitive::Sensitive;
use tokio::net::{TcpListener, TcpStream};

use tokio_util::codec::{Framed, LengthDelimitedCodec};

mod influx;
mod noise;

#[tokio::main(flavor = "current_thread")]
async fn main() -> color_eyre::Result<()> {
    tracing_subscriber::fmt::init();
    run().await
}

async fn run() -> color_eyre::Result<()> {
    let private_key = std::fs::read("server.key").unwrap();
    let private_key = Sensitive::<[u8; 32]>::from_slice(&private_key);
    let socket = TcpListener::bind("0.0.0.0:8010").await?;

    loop {
        let (client, addr) = socket.accept().await?;

        let key = private_key.clone();
        tokio::spawn(async move {
            if let Err(e) = try_handle_client(addr, client, key).await {
                tracing::warn!("{e}");
            }
        });
    }
}

#[tracing::instrument(skip(client, private_key))]
async fn try_handle_client(
    addr: SocketAddr,
    client: TcpStream,
    private_key: Sensitive<[u8; 32]>,
) -> color_eyre::Result<()> {
    let framed = Framed::new(client, LengthDelimitedCodec::new());

    tracing::info!("connection from");

    let mut stream = responder_handshake(framed, private_key).await?;

    tracing::info!("handshake complete");

    let msg = stream.read_message().await?;

    if let Ok(s) = core::str::from_utf8(&msg) {
        tracing::info!("{s}");
        influx::make_request(s.to_string()).await?;
    } else {
        tracing::warn!("undecodable");
    }


    Ok(())
}
