use color_eyre::eyre::{eyre, ContextCompat};
use futures_util::{SinkExt, StreamExt};
use noise_protocol::{patterns, CipherState, HandshakeState};
use noise_rust_crypto::{sensitive::Sensitive, Aes256Gcm, Sha256, X25519};
use tokio::net::TcpStream;
use tokio_util::{
    bytes::{Bytes, BytesMut},
    codec::{Framed, LengthDelimitedCodec},
};

pub async fn responder_handshake(
    mut stream: Framed<TcpStream, LengthDelimitedCodec>,
    private_key: Sensitive<[u8; 32]>,
) -> color_eyre::Result<NoiseStream> {
    let mut buffer = BytesMut::zeroed(1024);
    let mut state = HandshakeState::<X25519, Aes256Gcm, Sha256>::new(
        patterns::noise_xk(),
        false,
        &[],
        Some(private_key),
        None,
        None,
        None,
    );

    let frame = stream.next().await.context("eof")??;
    let pl1 = &mut buffer[..frame.len() - state.get_next_message_overhead()];
    state.read_message(&frame, pl1)?;

    let mut msg2 = buffer.split_to(state.get_next_message_overhead());
    state.write_message(&[], &mut msg2)?;
    stream.send(msg2.freeze()).await?;

    let frame = stream.next().await.context("eof")??;
    let pl2 = &mut buffer[..frame.len() - state.get_next_message_overhead()];
    state.read_message(&frame, pl2)?;

    assert!(state.completed());

    let (rx, tx) = state.get_ciphers();

    Ok(NoiseStream {
        stream,
        buffer,
        _tx: tx,
        rx,
    })
}

pub struct NoiseStream {
    stream: Framed<TcpStream, LengthDelimitedCodec>,
    buffer: BytesMut,
    _tx: CipherState<Aes256Gcm>,
    rx: CipherState<Aes256Gcm>,
}

impl NoiseStream {
    pub async fn read_message(&mut self) -> color_eyre::Result<Bytes> {
        let frame = self.stream.next().await.context("eof")??;

        let mut plain_frame = self.buffer.split_to(frame.len() - 16);
        self.rx
            .decrypt(&frame, &mut plain_frame)
            .map_err(|_| eyre!("decrypt"))?;

        Ok(plain_frame.freeze())
    }
}
